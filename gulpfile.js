var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');

gulp.task('jade', function(){
   return gulp.src(["*.jade", "dev/*.jade"])
       .pipe(jade({
           pretty: true
       }))
       .pipe(gulp.dest('release'))
});

gulp.task('sass', function () {
  return gulp.src('./dev/sass/**/*.scss')
    .pipe(sass({
        includePaths: ["bower_components/bootstrap-sass/assets/stylesheets/"]
      }).on('error', sass.logError))
    .pipe(gulp.dest('./release/css'));
});


gulp.task('build', ['jade', 'sass']);